# BeatBox-By-OneEyeX
BeatBox By OneEyeX is a music app that lets you try your own beat. Choose your musical style among 9 impressive atmospheres and start to lay down and enjoy your mix.

# You can TRY it using this link: https://oneeyex.github.io/BeatBox-By-OneEyeX/
